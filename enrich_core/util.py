import warnings as _warnings
import sys
from tempfile import mkdtemp
import re
import xlrd
import chardet
import time
import datetime
from os.path import join as join_path
from itertools import islice
from itertools import chain
import nltk
from geopy import geocoders
from geopy import distance
from PIL import Image
from threading import Thread
import pickle
import uuid as u
import unicodedata
import codecs
import zipfile
import os


def batch(iterable, size):
    sourceiter = iter(iterable)
    while True:
        batchiter = islice(sourceiter, size)
        yield chain([batchiter.next()], batchiter)


def log_print(s):
    print s


def parse_int(i, fallback=0):
    try:
        return int(i)
    except Exception:
        return fallback


def remove_non_ascii(s):
    return "".join(i for i in s if ord(i)<128)


def parse_emails_blob(blob_str):
    email_list = re.findall(r'[\w\.-]+@[\w\.-]+', blob_str)
    email_list = [email.lower() for email in email_list]
    return email_list


def extract_emails(filepath):
    emails = []
    file_extension = os.path.splitext(filepath)[1].lower()
    if file_extension == '.xls' or file_extension == '.xlsx':
        emails.extend(extract_emails_from_spreadsheet_filepath(filepath))
    else:
        emails.extend(extract_emails_from_text_filepath(filepath))
    return emails


def extract_emails_from_spreadsheet_filepath(filepath):
    book = xlrd.open_workbook(filepath)
    return extract_emails_from_spreadsheet_book(book)


def extract_emails_from_spreadsheet_fileobj(file):
    book = xlrd.open_workbook(file_contents=file.read())
    return extract_emails_from_spreadsheet_book(book)


def extract_emails_from_spreadsheet_book(book):
    emails = []
    for sheet in book.sheets():
        for row_index in range(0, sheet.nrows):
            row = sheet.row(row_index)
            for col_index in range(0, len(row)):
                col = row[col_index]
                if type(col.value) is unicode:
                    emails.extend(parse_emails_blob(col.value))
    return emails


def extract_emails_from_text_filepath(input_file):
    rawdata = open(input_file, "r").read()
    charenc = chardet.detect(rawdata)['encoding']
    try:
        blob = codecs.open(input_file, mode='r', encoding=charenc).read()
        emails = parse_emails_blob(blob)
    except Exception as e:
        print 'Error opening input file', str(e); return
    return emails


def extract_emails_from_text_filepath_no_chardet(input_file):
    blob = open(input_file).read()
    return parse_emails_blob(blob)


def extract_emails_from_text_fileobj(file):
    #rawdata = file.read() # assumes its an open file
    #charenc = chardet.detect(rawdata)['encoding']
    try:
        blob = file.read()
        emails = parse_emails_blob(blob)
    except Exception as e:
        print 'Error opening input file', str(e); return
    return emails


def utf8_encode(s):    
    return unicodedata.normalize('NFKD', unicode(s)).encode('ascii', 'ignore')


def create_dirs_if_necessary(dirs_path):
    if not os.path.exists(dirs_path):
        os.makedirs(dirs_path)


def date_to_int(date):
    return time.mktime(date.timetuple())


def date_from_int(integer):
    return datetime.datetime.fromtimestamp(integer)


def get_goelocation(location_str):
    time.sleep(0.2)  # there seems to be some kind of rate limit so we need this
    g = geocoders.GoogleV3()
    loc = None
    location_str1 = utf8_encode(location_str)
    try:
        loc = g.geocode(unicode(location_str1))
    except Exception as e:
        print 'exception when getting geocode: ', e
    if loc is None:
        if len([l for l in location_str1.split(' ') if l.lower() in ('greater', 'area')]) > 0:
            try:
                loc = g.geocode(unicode(location_str1))
            except Exception as e:
                print 'exception when getting geocode: ', e
    if loc is None:
        print 'util.get_geolocation() returned None for location: ', location_str1
    return loc


def geo_dist(geo1, geo2):
    if geo1 is None or geo2 is None:
        return None
    geo1 = geo1[1]
    geo2 = geo2[1]
    return distance.distance(geo1, geo2).km


def get_geo(location_str):
    g = geocoders.GoogleV3()
    return g.geocode(unicode(location_str))


def dist_between(location_str1, location_str2):
    time.sleep(0.5)  # seems to be a rate limit of 2500 per day
    g = geocoders.GoogleV3()

    location_str1 = utf8_encode(location_str1)
    location_str2 = utf8_encode(location_str2)

    loc1 = g.geocode(unicode(location_str1))
    loc2 = g.geocode(unicode(location_str2))

    if loc1 is None or loc2 is None:
        return None

    place, ne = loc1
    place, cl = loc2

    dist = distance.distance(ne, cl).km
    return dist


def levenshtein_dist(s1, s2):
    return nltk.metrics.edit_distance(s1, s2)


def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)


def scale(val, src, dst, floor=None, ceiling=None):
    """Scale the given value from the scale of src to the scale of dst."""
    val = float(val)
    ans = ((val - src[0]) / (src[1]-src[0])) * (dst[1]-dst[0]) + dst[0]
    if floor and ans < floor:
        ans = floor
    if ceiling and ans > ceiling:
        ans = ceiling
    return ans


def get_chain(dictionary, keys_tuple):
        keys = keys_tuple
        working_dict = dictionary
        for key in keys:
            if working_dict.get(key) is None:
                return None
            working_dict = working_dict[key]
        return working_dict


class ChainDict(dict):
    def _get_chain(self, *args):
        """Extracts a value from a series of nested dictionaries"""
        keys = args
        working_dict = self
        for key in keys:
            if working_dict.get(key) is None:
                return None
            working_dict = working_dict[key]
        return working_dict


def remove_duplicates(li, func):  
    i = 0
    while i < len(li):
        for j in range(i + 1, len(li)):
            if func(li[i]) == func(li[j]):
                del li[i]
                i -= 1
                break
        i += 1


def zip_files(filepath_list, zip_name, target_dir):
    zip_file = zipfile.ZipFile(zip_name, 'w')
    for filepath in filepath_list:
        zip_file.write(filepath, get_filename(filepath), zipfile.ZIP_DEFLATED)
    zip_file.close()
    os.rename(zip_name, join_path(target_dir, zip_name))
    return join_path(target_dir, zip_file)


def create_cropped(img_path, x, y, width, height, dest_path=None):
    img = Image.open(img_path)

    # ensure crop box is within bounds and in the form of integers
    img_width, img_height = img.size
    x, y = clamp(x, 0, img_width), clamp(y, 0, img_height)
    x2, y2 = clamp(x + width, 0, img_width), clamp(y + height, 0, img_height)
    x, x2, y, y2 = int(round(x)), int(round(x2)), int(round(y)), int(round(y2))

    box = (x, y, x2, y2)  # box = (x, y, x + width, y + height)
    cropped_img = img.crop(box)
    if dest_path is None:
        dest_path = img_path[:img_path.rfind('/') + 1] + get_filename(img_path, with_ext=False) + '_scaled.jpeg'
    cropped_img.save(dest_path, 'jpeg')
    return dest_path


def get_filename(filepath, with_ext=True):
    last_slash_pos = filepath.rfind('/')  # warning: not portable
    ans = filepath if last_slash_pos == -1 else filepath[last_slash_pos + 1:]
    if with_ext:
        return ans
    else:
        return os.path.splitext(ans)[0]


class ThreadPool(object):
    def __init__(self, max_threads):
        if max_threads < 1:
            print 'warning: thread pool < 1 number of threads'
        self.max_threads = max_threads
        self.threads = []

    def submit(self, func, *args, **kwargs):
        if len(self.threads) >= self.max_threads:
            while len(self.threads) >= self.max_threads:
                i = 0
                while i < len(self.threads):
                    if not self.threads[i].isAlive():
                        del self.threads[i]
                        i -= 1
                    i += 1
                if len(self.threads) >= self.max_threads:
                    time.sleep(1)
                    print 'thread pool sleeping'
        t = Thread(target=func, *args, **kwargs)
        self.threads.append(t)
        t.start()

    def shutdown(self):
        for t in self.threads:
            t.join()


class TemporaryDirectory(object):
    """Create and return a temporary directory.  This has the same
    behavior as mkdtemp but can be used as a context manager.  For
    example:

        with TemporaryDirectory() as tmpdir:
            ...

    Upon exiting the context, the directory and everything contained
    in it are removed.
    Taken from: http://stackoverflow.com/questions/19296146/with-tempfile-temporarydirectory
    """
    def __init__(self, suffix="", prefix="tmp", dir=None):
        self._closed = False
        self.name = None # Handle mkdtemp raising an exception
        self.name = mkdtemp(suffix, prefix, dir)

    def __repr__(self):
        return "<{} {!r}>".format(self.__class__.__name__, self.name)

    def __enter__(self):
        return self.name

    def get_path(self):  # or could add key 'path' to getattr
        return self.name

    def cleanup(self, _warn=False):
        if self.name and not self._closed:
            try:
                self._rmtree(self.name)
            except (TypeError, AttributeError) as ex:
                # Issue #10188: Emit a warning on stderr
                # if the directory could not be cleaned
                # up due to missing globals
                if "None" not in str(ex):
                    raise
                print "ERROR: {!r} while cleaning up {!r}".format(ex, self,)
                #old: print("ERROR: {!r} while cleaning up {!r}".format(ex, self,), file=sys.stderr)
                return
            self._closed = True
            if _warn:
                pass  #old: self._warn("Implicitly cleaning up {!r}".format(self), ResourceWarning)

    def __exit__(self, exc, value, tb):
        self.cleanup()

    def __del__(self):
        # Issue a ResourceWarning if implicit cleanup needed
        self.cleanup(_warn=True)

    # XXX (ncoghlan): The following code attempts to make
    # this class tolerant of the module nulling out process
    # that happens during CPython interpreter shutdown
    # Alas, it doesn't actually manage it. See issue #10188
    _listdir = staticmethod(os.listdir) # used to be: _os.listdir (the os import had an 'as _os')
    _path_join = staticmethod(os.path.join)
    _isdir = staticmethod(os.path.isdir)
    _islink = staticmethod(os.path.islink)
    _remove = staticmethod(os.remove)
    _rmdir = staticmethod(os.rmdir)
    _warn = _warnings.warn

    def _rmtree(self, path):
        # Essentially a stripped down version of shutil.rmtree.  We can't
        # use globals because they may be None'ed out at shutdown.
        for name in self._listdir(path):
            fullname = self._path_join(path, name)
            try:
                isdir = self._isdir(fullname) and not self._islink(fullname)
            except OSError:
                isdir = False
            if isdir:
                self._rmtree(fullname)
            else:
                try:
                    self._remove(fullname)
                except OSError:
                    pass
        try:
            self._rmdir(path)
        except OSError:
            pass


name_equivalents = (
    ('david', 'dave'),
    ('michael', 'mike'),
)
name_equivs_dict = dict(name_equivalents); name_equivs_dict.update([(v,k) for (k,v) in name_equivalents])

EXACT_NAME_MATCH = 1.0
PARTIAL_NAME_MATCH = 0.9
NOT_NAME_MATCH = 0.2


def compare_names(linkedin_name, fc_name):  #, min_val=0.2):

    linkedin_name, fc_name = linkedin_name.lower().strip(), fc_name.lower().strip()

    if linkedin_name == fc_name:
        return EXACT_NAME_MATCH

    def replace_equivs(name):
        for i, word in enumerate(name.split(' ') or []):
            if word in name_equivs_dict:
                return name.replace(word, name_equivs_dict[word])
        return name

    if linkedin_name == replace_equivs(fc_name) or fc_name == replace_equivs(linkedin_name):
        print 'partial name match - when name-equivalent replaced', linkedin_name, fc_name
        return PARTIAL_NAME_MATCH

    def removeDr(name):
        return name[name.find('dr.')+3:].strip() if name.startswith('dr.') else name

    if linkedin_name == removeDr(fc_name) or fc_name == removeDr(linkedin_name):
        print 'partial name match - when dr. removed: ', linkedin_name, fc_name
        return PARTIAL_NAME_MATCH

    def removeMiddleName(name):
        return name.split(' ')[0] + ' ' + name.split(' ')[2] if len(name.split(' ')) == 3 else name

    if linkedin_name == removeMiddleName(fc_name) or fc_name == removeMiddleName(linkedin_name):
        print 'partial name match - when middle name remvoved: ', linkedin_name, fc_name
        return PARTIAL_NAME_MATCH

    def reverse_names(name):
        return name.split(' ')[1] + ' ' + name.split(' ')[0] if len(name.split(' ')) == 2 else name

    if linkedin_name == reverse_names(fc_name) or fc_name == reverse_names(linkedin_name):
        print 'partial name match - when reversed names: ', linkedin_name, fc_name
        return PARTIAL_NAME_MATCH

    return NOT_NAME_MATCH


def pickle_load(filepath):
    if not os.path.isfile(filepath):
        print 'warning: no file found by pickle_load()'
        return None
    f = open(filepath, 'rb')
    result = pickle.load(f)
    f.close()
    return result


def pickle_dump(data_obj, filepath):
    pickle_file = open(filepath, 'wb')
    pickle.dump(data_obj, pickle_file)
    pickle_file.close()


def uuid():
    return u.uuid4().hex