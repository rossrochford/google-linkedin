import json
import requests
import enrich_core.util as util
import time
import linkedin_core


search_types = {
    'linkedin': {
        'search_engine_id': linkedin_search_engine_id,
        'item_class': linkedin_core.LinkedInGoogleResultItem
    }
}


def correct_query_string(query_str):
    query_str = query_str.strip().replace('  ', ' ')
    query_str = query_str.replace('   ', ' ')
    query_str = query_str.replace(' ', '+')
    return query_str


def query_google(query, search_type, max_results=10):
    query = correct_query_string(query)

    se_id = search_type[search_type]['search_engine_id']
    base_url = 'https://www.googleapis.com/customsearch/v1?q=%s&cx=%s&key=%s' % (query, se_id, browser_key)

    ItemClass = search_types[search_type]['item_class']

    results = []
    while len(results) < min(max_results, 1000):   # google api limits us to 1000 results
        request_url = base_url + '&start=%s&num=%s' % (len(results) + 1, 10)

        try:
            resp = requests.get(request_url)
        except Exception as e:
            print 'Exception querying google: ', e
            return results
        if resp.status_code >= 400:
            # wait and try once more
            time.sleep(4)
            try:
                resp = requests.get(request_url)
            except Exception as e:
                return results
            if resp.status_code >= 400:
                print 'Google query disallowed.'
                return results

        json_resp = json.loads(resp.content)

        for json_item in json_resp.get('items', []):
            position = len(results)
            total_results = json_resp.get('searchInformation', {}).get('totalResults')

            item = ItemClass(json_item, google_pos=position, total_results=total_results)
            results.append(item)

        if len(json_resp.get('items', [])) < 10:
            break  # end of result-set

    return results[:max_results]



