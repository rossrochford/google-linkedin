import os
import urllib


import enrich_core.util as util
import mess.image_sim_test2 as img_comp
from google_core import query_google
from linkedin import linkedin


authentication = linkedin.LinkedInDeveloperAuthentication(
    LINKEDIN_CONSUMER_KEY,
    LINKEDIN_CONSUMER_SECRET,
    LINKEDIN_USER_TOKEN,
    LINKEDIN_USER_SECRET,
    'http://www.google.com',
    linkedin.PERMISSIONS.enums.values()
)

linkedin_app = linkedin.LinkedInApplication(authentication)

# shouldn't be hard-coded here, I know
english_stopwords = [
    'i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves',
    'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'they', 'them', 'their',
    'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was',
    'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the',
    'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against',
    'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in',
    'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why',
    'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only',
    'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should', 'now'
]

# anything with these urls are not linkedin profiles
exclude_urls_with = (
    '/dir/', '/groups', '/influencer/', '/today/article', '/company/', '/directory/', '/job/', '/jobs2/', '/title/',
    '/in/updates'
)

# Ranking parameters
POSITION_WEIGHT = 1.0
LOCATION_WEIGHT = 0.8
KEYWORD_WEIGHT = 0.1
NAME_MATCH_WEIGHT = 1.0
EXACT_LOCATION_BONUS_FACTOR = 1.1
NAME_PLUS_LOCATION_BONUS_FACTOR = 1.3
EXACT_LOCATION_DIST = 15  # anything within 10 km is considered an exact location match
IMAGE_MATCH_THRESHOLD = 4200  # I've decided that 4200 is a reasonable value


class LinkedInGoogleResultItem(util.ChainDict):

    image_cache_dir = '/Users/rossrochford/Documents/workspace-pycharm/riches2/riches/mess/downloaded_images'

    def __init__(self, google_pos=None, total_results=None, *args, **kwargs):
        self.google_position = google_pos
        self.total_results = total_results
        self.rank = None
        self.headline = None
        self.geolocation = None
        self.cached_image_path = None
        super(LinkedInGoogleResultItem, self).__init__(*args, **kwargs)

    def get_page_title(self):
        return unicode(self.get('title'))

    def get_name(self):
        name = self.get_page_title()[:self.get_page_title().rfind('| L')-1].strip()
        if ' - ' in name:
            name = name[:name.rfind(' - ')]
        return name

    # there seem to be 3 areas you can get the role from
    def get_role(self):
        if self._get_role_from_headline():
            return self._get_role_from_headline()
        if self._get_role_from_person_entry():
            return self._get_role_from_person_entry()
        if self._get_role_from_hcard_entry():
            return self._get_role_from_hcard_entry()
        return None

    def _get_role_from_headline(self):
        headline = self.get_headline()
        if headline is None:
            return None
        if headline.rfind(' at ') != -1:
            return headline[:headline.rfind(' at ')]
        return headline

    def _get_role_from_person_entry(self):
        person_entry = self._get_chain('pagemap', 'person')
        if person_entry is None:
            return None
        return person_entry[0].get('role')

    def _get_role_from_hcard_entry(self):
        hcard_entry = self._get_chain('pagemap', 'hcard')
        if hcard_entry is None:
            return None
        return hcard_entry[0].get('title')

    def get_current_company(self):
        headline = self.get_headline()
        if headline is None:
            return None
        if headline.rfind(' at ') != -1:
            return headline[headline.rfind(' at ')+4:]
        return None

    def get_headline(self, force_linkedin_query=False):
        if self.headline:
            return self.headline
        if not force_linkedin_query and self._get_chain('pagemap', 'person'):
            self.headline = self._get_chain('pagemap', 'person')[0].get('role')
            return self.headline
        if is_profile_url(self.get_url()):
            print 'querying linkedin directly for headline'
            try:
                profile = linkedin_app.get_profile(member_url=self.get_url())
            except Exception as e:
                print 'exception when querying linkedin: ', e
                profile = {}
            self.headline = profile.get('headline')
            if self.headline is None:
                # fall back on headline from google
                self.headline = self._get_chain('pagemap', 'person')[0].get('role')
        return self.headline

    def get_google_cache_url(self):
        return 'http://webcache.googleusercontent.com/search?q=cache:' + self.get_url()

    def get_image_url(self):
        if self._get_chain('pagemap', 'cse_image') is None:
            ans = self._get_thumbnail_url()  # though its probably None
        else:
            ans = self._get_chain('pagemap', 'cse_image')[0].get('src')
        return ans

    def _get_thumbnail_url(self):
        if self._get_chain('pagemap', 'cse_thumbnail') is None:
            return None
         # or hcard[0]['photo']  (but this requests from linkedin instead of google's cache)
        return self._get_chain('pagemap', 'cse_thumbnail')[0].get('src')

    def get_url(self):
        return self.get('link')

    def get_location(self):
        if self._get_chain('pagemap', 'person'):
            return self._get_chain('pagemap', 'person')[0].get('location')
        return None

    def get_image(self):
        """
        Returns path of local copy of linkedin profile image. If the image hasn't already been cached, it will
        be downloaded and saved in image_cache_dir
        """
        if self.cached_image_path:
            return self.cached_image_path
        if self.get_image_url() is None or self.cached_image_path == 'CHECKED':
            return None
        dest = download_image2(self.get_image_url(), self.image_cache_dir)
        if dest is None:
            self.cached_image_path = 'CHECKED'
            return None
        self.cached_image_path = dest
        return self.cached_image_path

    def get_face_cropped_image(self):
        if self.cropped_image_path:
            return self.cropped_image_path
        if self.get_image_url() is None or self.cropped_image_path == 'CHECKED':
            return None
        face = get_url_face(self.get_image_url())
        if face is None:
            self.cropped_image_path = 'CHECKED'
            return None
        self.cropped_image_path = util.create_cropped(
            self.get_image(), face['x'], face['y'], face['width'], face['height']
        )
        return self.cropped_image_path

    def get_geolocation(self, cached_geolocations={}):
        """returns geocoded location (string name and long/lat) and caches it in cached_geolocations"""
        if self.get_location() is None:
            return None
        if self.geolocation is not None:
            return self.geolocation
        if cached_geolocations and self.get_location() in cached_geolocations:
            return cached_geolocations.get(self.get_location())
        self.geolocation = util.get_goelocation(self.get_location())
        cached_geolocations[self.get_location()] = self.geolocation
        return self.geolocation

    def get_universities(self):
        universities = []
        for card_item in self._get_chain('pagemap', 'hcard') or []:
            if card_item.get('fn') and 'university' in card_item['fn'].lower():
                universities.append(card_item['fn'])
        return universities

    def get_groups(self):
        groups = []
        for card_item in self._get_chain('pagemap', 'hcard') or []:
            groups.append(card_item['fn'])
        universities = self.get_universities()
        groups = [g for g in groups if g not in universities]
        name = self.get_name()
        groups = [g for g in groups if (name or '').lower() not in g.lower()]
        return groups

    def get_keyword_bag(self):
        groups = self.get_groups()
        keywords = []
        for group in self.get_groups() or []:
            keywords.extend(group.split(' '))
        if self.get_headline():
            keywords.extend(self.get_headline().split(' '))
        keywords = [word.lower() for word in keywords]
        keywords = list(set(keywords) - set(english_stopwords))
        return keywords

    def __eq__(self, other):
        return linkedin_urls_equal(self.get_url(), other.get_url()) and self.get_name() == self.get_name()

    def __hash__(self):
        return hash((self.get_name(), self.get_url()))

    def get_rank(self, context_dict, cached_geolocations={}, cached_image_sims={}):
        """
        crude algorithm for ranking linkedin results to pick the most likely result item given the information we
        know about the person (name, images, location, keywords). What we really need here is to build a bayesian
        classifier
        """
        if self.rank is not None: # we cache the rank for performance reasons
            return self.rank

        position_factor = util.scale(self.google_position, (0, 19), (1.0, 0.25), floor=0.1, ceiling=1.0)

        if context_dict.get('keywords') and len(self.get_keyword_bag()) > 1:
            linkedin_keywords = self.get_keyword_bag()
            num_matches = sum([1 for kw in linkedin_keywords if kw in context_dict['keywords']])
            keyword_factor = num_matches / float(len(linkedin_keywords))
        else:
            keyword_factor = 0.4

        dist_km = None
        if context_dict.get('geolocation') and self.get_geolocation(cached_geolocations=cached_geolocations):
            dist_km = util.geo_dist(
                context_dict['geolocation'], self.get_geolocation(cached_geolocations=cached_geolocations)
            )
            if dist_km:
                location_factor = util.scale(dist_km, (0, 300), (1.0, 0.1), floor=0.15, ceiling=1.0)
                if dist_km <= EXACT_LOCATION_DIST:
                    location_factor += EXACT_LOCATION_BONUS_FACTOR
            else:
                location_factor = 0.4
        else:
            location_factor = 0.4

        if context_dict.get('name') and self.get_name():
            name_factor = util.compare_names(self.get_name(), context_dict['name'])
        else:
            name_factor = 0.4

        if name_factor == 1.0 and dist_km is not None and dist_km <= EXACT_LOCATION_DIST:
            self.rank *= NAME_PLUS_LOCATION_BONUS_FACTOR

        # weighted average
        self.rank = (
            (POSITION_WEIGHT * position_factor) + (LOCATION_WEIGHT * location_factor) +
            (KEYWORD_WEIGHT * keyword_factor) + (NAME_MATCH_WEIGHT * name_factor)
        ) / (POSITION_WEIGHT + LOCATION_WEIGHT + KEYWORD_WEIGHT + NAME_MATCH_WEIGHT)

        if context_dict.get('images') and self.get_image():

            max_image_similarty, max_matching_fc_image_path, max_matching_fc_image_source = 0, None, None

            for img_src, img_list in context_dict['images'].items():
                for fc_img in img_list:
                    if not os.path.exists(fc_img):
                        continue  # should never happen
                    if (self.get_image(), fc_img) not in cached_image_sims:
                        image_similarity = img_comp.compute_image_similarity(self.get_image(), fc_img)
                        cached_image_sims[self.get_image(), fc_img] = image_similarity
                    sim = cached_image_sims[self.get_image(), fc_img]
                    if sim > max_image_similarty:
                        max_image_similarty = sim

            if max_image_similarty > IMAGE_MATCH_THRESHOLD:
                self.rank = 100 if name_factor >= 0.9 else 50

        return self.rank

    def __repr__(self):
        return u'Name: ' + unicode(self.get_name()) + \
               u'\nPage Title:' + unicode(self.get_page_title()) + \
               u'\nHeadline: ' + unicode(self.get_headline()) + \
               u'\nRole: ' + unicode(self.get_role()) + \
               u'\nUrl: ' + unicode(self.get_url()) + \
               u'\nThumbnail: ' + unicode(self.get_image_url()) + \
               u'\nCompany: ' + unicode(self.get_current_company()) + \
               u'\nLocation: ' + unicode(self.get_location()) + \
               u'\nUniversities: ' + unicode(self.get_universities()) + \
               u'\nGroups: ' + unicode(self.get_groups()) + \
               u'\nKeywords: ' + unicode(self.get_keyword_bag())
               #u'\nRank: ' + unicode(self.get_rank())


def is_profile_url(url):
    return len([ex for ex in exclude_urls_with if ex in url]) == 0 and '/in/' in url


def google_linkedin(query_string, num_results=20):
    linkedin_results = query_google(query_string, 'linkedin', max_results=num_results)
    linkedin_results = [lp for lp in linkedin_results if lp.get_url() and is_profile_url(lp.get_url())]
    return linkedin_results


def download_image2(url, dest_dir):
    image = urllib.URLopener()
    res = None
    try:
        res = image.retrieve(url)
    except Exception as e:
        if e.args[1] == 301:  # a redirect
            new_location = [l for l in e.args[3].headers if l.lower().startswith('location: ')][0][9:].strip()
            try:
                res = image.retrieve(new_location)
            except Exception as e:
                return None
    if res is None:
        return None
    img_path = res[0]
    extension = res[1].subtype
    if extension.lower() == 'jpg':
        extension = 'jpeg'
    filename = util.randString(10) + '.' + extension
    dest = os.path.abspath(dest_dir + '/' + filename)
    os.rename(img_path, dest)
    return dest 


def linkedin_urls_equal(url1, url2):

    url1 = url1.strip().lower()
    url1 = url1[url1.find('linkedin.'):]

    start_pos = url1.find('/in/') + 4 if url1.find('/in/') > 0 else url1.find('/pub/') + 5
    end_pos = url1.rfind('/') if url1.rfind('/') > start_pos else len(url1)
    username1 = url1[start_pos:end_pos]

    url2 = url2.strip().lower()
    url2 = url2[url2.find('linkedin.'):]

    start_pos = url2.find('/in/') + 4 if url2.find('/in/') > 0 else url2.find('/pub/') + 5
    end_pos = url2.rfind('/') if url2.rfind('/') > start_pos else len(url2)
    username2 = url2[start_pos:end_pos]

    if '/profile/view?id=' in url1:
        print 'url with /profile/view?id= found:', url1
    if '/profile/view?id=' in url2:
        print 'url with /profile/view?id= found:', url2

    if url1 != url2 and (username1 == username2): # or ids_equal)
        print 'urls not equal but profiles considered the same:', url1, url2

    return url1 == url2 or username1 == username2 #or ids_equal